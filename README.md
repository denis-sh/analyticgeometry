﻿Analytic geometry
===================================
Simple 2D/3D analytic geometry library.

Status
-----------------------------------
The project is used by its author so it unlikely contains obvious bugs.
Project structure isn't ready yet so **API is subject to change**.
Also this is `dev` branch which revision history is temporary and may be changed.

So it's strongly recommended to contact the author if you want to use it seriously and you aren't a masochist.

Completeness
-----------------------------------
Due to lack of time only required for the application the project is used in functionality is implemented.

License
-----------------------------------
The project is licensed under the terms of the [Boost Software License, Version 1.0](http://boost.org/LICENSE_1_0.txt).
