﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in3d.vector;


import std.math; // sqrt, atan2, acos

import geometry.in2d.angle;
import geometry.in3d.point;


@safe pure nothrow @nogc:

struct Vector
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	real x, y, z;

	// Constants
	// ----------------------------------------------------------------------------------------------------

	enum Vector nan = Vector(real.nan, real.nan, real.nan), zero = Vector(0, 0, 0);

	enum Vector i = Vector(1, 0, 0), j = Vector(0, 1, 0), k = Vector(0, 0, 1);

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in real x, in real y, in real z) { this.x = x; this.y = y; this.z = z; }

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		real isZero() { return x == 0 && y == 0 && z == 0; }

		real lengthSq() { return x * x + y * y + z * z; }

		real length() { return sqrt(lengthSq); }

		Angle angleXY() { return Angle.fromYX(y, x); }

		Angle angleYZ() { return Angle.fromYX(z, y); }

		Angle angleZX() { return Angle.fromYX(x, z); }

		real angleSignedValueXY() { return atan2(y, x); }

		real angleSignedValueYZ() { return atan2(z, y); }

		real angleSignedValueZX() { return atan2(x, z); }

		Vector normalized() { return opBinary!"/"(length); }

		Point asPoint() { return Point(this.tupleof); }
	}

	@property
	{
		real length(in real value)
		{
			opOpAssign!"*"(value / length);
			return value;
		}
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Vector opUnary(string op : "-")() { return Vector(-x, -y, -z); }

		Vector opUnary(string op : "+")() { return this; }

		Vector opBinary(string op : "+")(in Vector v) { return Vector(x + v.x, y + v.y, z + v.z); }

		Vector opBinary(string op : "-")(in Vector v) { return Vector(x - v.x, y - v.y, z - v.z); }

		Vector opBinary(string op : "*")(in real n) { return Vector(x * n, y * n, z * n); }

		Vector opBinaryRight(string op : "*")(in real n) { return opBinary!"*"(n); }

		Vector opBinary(string op : "/")(in real n) { return Vector(x / n, y / n, z / n); }

		/// Dotproduct
		real opBinary(string op : "*")(in Vector v) { return x * v.x + y * v.y + z * v.z; }
	}

	ref Vector opOpAssign(string op : "+")(in Vector v) { x += v.x; y += v.y; z += v.z; return this; }

	ref Vector opOpAssign(string op : "-")(in Vector v) { x -= v.x; y -= v.y; z -= v.z; return this; }

	ref Vector opOpAssign(string op : "*")(in real n) { x *= n; y *= n; z *= n; return this; }

	ref Vector opOpAssign(string op : "/")(in real n) { x /= n; y /= n; z /= n; return this; }

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		/// [a, b]
		Vector crossproduct(in Vector v) { return Vector(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x); }

		/// (a, b, c) = (a, [b,c])
		real tripleProduct(in Vector v1, in Vector v2) { return this * v1.crossproduct(v2); }

		real parallelComponentToLengthSq(in Vector v) { return opBinary!"*"(v) ^^ 2 / v.lengthSq; }

		Vector parallelComponentTo(in Vector v) { return v * (opBinary!"*"(v) / v.lengthSq); }

		Vector normalComponentTo(in Vector v) { return opBinary!"-"(parallelComponentTo(v)); }

		Vector withLength(in real value) { return opBinary!"*"(value / length); }

		Angle angleWith(in Vector v)
		{
			const k = sqrt(lengthSq * v.lengthSq);
			return Angle(crossproduct(v).length / k, opBinary!"*"(v) / k);
		}

		real angleValueWith(in Vector v) { return acos(angleWith(v).cos); }

		Vector rotated(in Vector axis, in Angle angle)
		{
			Vector res = this;
			res.rotate(axis, angle);
			return res;
		}
	}

	void setSameLength(in Vector v) { opOpAssign!"*"(sqrt(v.lengthSq / lengthSq)); }

	void normalize() { opOpAssign!"/"(length); }

	void invert() { x = -x; y = -y; z = -z; }

	void rotate(in Vector axis, in Angle angle)
	in { assert(!axis.isZero); }
	body
	{
		const bn0 = crossproduct(axis);
		if(bn0.isZero)
			return;
		const bn = bn0.normalized, n = axis.crossproduct(bn).normalized;
		const d = opBinary!"*"(n);
		// x = d (along n), y = 0 (along bn)
		const u = d * angle.cos;
		const v = d * angle.sin;
		opOpAssign!"+"((u - d) * n + v * bn);
	}

	// TODO: all approx, rotateZTo?, xy (ref?), yz (ref?), zx
}
