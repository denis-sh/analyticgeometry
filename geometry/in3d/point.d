﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in3d.point;


import std.math; // sqrt

import geometry.in3d.vector;
import geometry.in3d.box;


@safe pure nothrow @nogc:

struct Point
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	real x, y, z;

	// Constants
	// ----------------------------------------------------------------------------------------------------

	enum Point nan = Point(real.nan, real.nan, real.nan), origin = Point(0, 0, 0);

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in real x, in real y, in real z) { this.x = x; this.y = y; this.z = z; }

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static
	{
		Point midpoint(in Point point1, in Point point2)
		{ return ((point1.asVector + point2.asVector) / 2).asPoint; }
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		Box box()
		{ return Box(this, this); }

		Vector asVector() { return Vector(this.tupleof); }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Point opBinary(string op : "+")(in Vector v)
		{ return Point(x + v.x, y + v.y, z + v.z); }

		Point opBinary(string op : "-")(in Vector v)
		{ return Point(x - v.x, y - v.y, z - v.z); }

		Vector opBinary(string op : "-")(in Point p)
		{ return Vector(x - p.x, y - p.y, z - p.z); }
	}

	ref Point opOpAssign(string op : "+")(in Vector v)
	{ x += v.x; y += v.y; z += v.z; return this; }

	ref Point opOpAssign(string op : "-")(in Vector v)
	{ x -= v.x; y -= v.y; z -= v.z; return this; }

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		real distanceSq(in Point p) { return opBinary!"-"(p).lengthSq; }

		real distance(in Point p) { return sqrt(distanceSq(p)); }
	}
}
