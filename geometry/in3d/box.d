﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in3d.box;


import std.algorithm; // min, max

import geometry.in3d.size;
import geometry.in3d.vector;
import geometry.in3d.point;


@safe pure nothrow @nogc:

struct Box
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Point start, end;

	invariant()
	{ assert(start.x <= end.x && start.y <= end.y && start.z <= end.z); }

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in real x1, in real y1, in real z1, in real x2, in real y2, in real z2)
	{ this.start = Point(min(x1, x2), min(y1, y2), min(z1, z2)); this.end = Point(max(x1, x2), max(y1, y2), max(z1, z2)); }

	this(in Point p1, in Point p2)
	{ this(p1.tupleof, p2.tupleof); }

	this(in Point point, in Vector vector)
	{ this(point, point + vector); }

	this(in Point point, in SizeR size)
	{ this(point, point + Vector(size.tupleof)); }

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static
	{
		Box fromCentre(in Point centre, in SizeR size)
		{ return Box(centre - Vector(size.tupleof) / 2, size); }
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		real width() { return end.x - start.x; }

		real height() { return end.y - start.y; }

		real depth() { return end.z - start.z; }

		Vector diagonal() { return end - start; }

		SizeR size() { return SizeR(diagonal.tupleof); }

		Point centre() { return ((start.asVector + end.asVector) / 2).asPoint; }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Box opBinary(string op : "&")(in Box other)
		in { assert(crosses(other)); }
		body { return Box(max(start.x, other.start.x), max(start.y, other.start.y), max(start.z, other.start.z), min(end.x, other.end.x), min(end.y, other.end.y), min(end.z, other.end.z)); }

		Box opBinary(string op : "|")(in Box other)
		{ return Box(min(start.x, other.start.x), min(start.y, other.start.y), min(start.z, other.start.z), max(end.x, other.end.x), max(end.y, other.end.y), max(end.z, other.end.z)); }

		Box opBinary(string op : "|")(in Point p)
		{ return Box(min(start.x, p.x), min(start.y, p.y), min(start.z, p.z), max(end.x, p.x), max(end.y, p.y), max(end.z, p.z)); }
	}

	ref Box opOpAssign(string op : "&")(in Box other)
	in { assert(crosses(other)); }
	body
	{
		start = Point(max(start.x, other.start.x), max(start.y, other.start.y), max(start.z, other.start.z));
		end = Point(min(end.x, other.end.x), min(end.y, other.end.y), min(end.z, other.end.z));
		return this;
	}

	ref Box opOpAssign(string op : "|")(in Box other)
	{
		start = Point(min(start.x, other.start.x), min(start.y, other.start.y), min(start.z, other.start.z));
		end = Point(max(end.x, other.end.x), max(end.y, other.end.y), max(end.z, other.end.z));
		return this;
	}

	ref Box opOpAssign(string op : "|")(in Point p)
	{
		start = Point(min(start.x, p.x), min(start.y, p.y), min(start.z, p.z));
		end = Point(max(end.x, p.x), max(end.y, p.y), max(end.z, p.z));
		return this;
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		bool contains(in Point p)
		{ return p.x >= start.x && p.y >= start.y && p.z >= start.z && p.x <= end.x && p.y <= end.y && p.z <= end.z; }

		bool crosses(in Box other)
		{ return start.x <= other.end.x && end.x >= other.start.x && start.y <= other.end.y && end.y >= other.start.y && start.z <= other.end.z && end.z >= other.start.z; }

		// TODO: distanceSq
	}
}
