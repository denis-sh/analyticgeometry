﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in3d.surfaces.plane;


import geometry.in3d.vector;
import geometry.in3d.point;
import geometry.in3d.curves.line;


@safe pure nothrow @nogc:

struct Plane
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Point point;
	Vector normal;

	invariant()
	{ assert(!normal.isZero); }

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in Point point, in Vector normal)
	{ this.point = point; this.normal = normal; }

	this(in Point point, in Vector vector1, in Vector vector2)
	{ this(point, vector1.crossproduct(vector2)); }

	this(in Point point1, in Point point2, in Point point3)
	{ this(point1, point2 - point1, point3 - point1); }

	this(in Point point, in Line line)
	{ this(point, line.point, line.point + line.vector); }

	this(in Vector normal, in real d)
	{ this((normal * (-d / normal.lengthSq)).asPoint, normal); }

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static
	{
		Plane equidistant(in Point point1, in Point point2)
		{ return Plane(Point.midpoint(point1, point2), point2 - point1); }
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		real d() { return -(normal * point.asVector); }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Line opBinary(string op : "&")(in Plane other)
		in { assert(!normal.crossproduct(other.normal).isZero); }
		body
		{
			const bn = normal.crossproduct(other.normal);
			const v = bn.crossproduct(normal);
			const x = (other.point - point) * other.normal / (v * other.normal);
			return Line(point + v * x, bn);
		}

		Point opBinary(string op : "&")(in Line line)
		in { assert(normal * line.vector); }
		body
		{
			const t = -(normal * line.point.asVector + d) / (normal * line.vector);
			return line.point + line.vector * t;
		}
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		real distanceSq(in Point p)
		{ return (normal * (p - point)) ^^ 2 / normal.lengthSq; }

		real signedDistance(in Point p)
		{ return normal.normalized * (p - point); }

		Vector projectionVector(in Point p)
		{ return normal * (normal * (p - point)) / -normal.lengthSq; }

		Point projection(in Point p)
		{ return p + projectionVector(p); }
	}
}
