﻿/**
3D curves package module.

Copyright: Denis Shelomovskij 2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in3d.curves;


public:
import geometry.in3d.curves.line;
import geometry.in3d.curves.ray;
import geometry.in3d.curves.segment;
