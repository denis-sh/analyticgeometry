﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in3d.curves.line;


import geometry.in3d.vector;
import geometry.in3d.point;


@safe pure nothrow @nogc:

struct Line
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Point point;
	Vector vector;

	invariant()
	{ assert(!vector.isZero); }

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in Point point, in Vector vector)
	{ this.point = point; this.vector = vector; }

	this(in Point point1, in Point point2)
	{ this(point1, point2 - point1); }

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		real distanceSq(in Point p)
		{ return (p - point).crossproduct(vector).lengthSq / vector.lengthSq; }

		real distanceSq(in Line l)
		{
			const normal = vector.crossproduct(l.vector);
			if(!normal.isZero)
				return (l.point - point).parallelComponentToLengthSq(normal);
			return distanceSq(l.point);
		}

		// TODO: getMindistPoints

		Vector projectionVector(in Point p)
		{ return (point - p).normalComponentTo(vector); }

		Line normal(in Point p)
		{ return Line(p, projectionVector(p)); }

		Point projection(in Point p)
		{ return p + projectionVector(p); }
	}
}
