﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in3d.curves.ray;


import geometry.in3d.vector;
import geometry.in3d.point;
import geometry.in3d.curves.line;


@safe pure nothrow @nogc:

struct Ray
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Point start;
	Vector vector;

	invariant()
	{ assert(!vector.isZero); }

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in Point start, in Vector vector)
	{ this.start = start; this.vector = vector; }

	this(in Point start, in Point point)
	{ this(start, point - start); }

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		Line baseLine()
		{ return Line(start, vector); }
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		real distanceSq(in Point p)
		{
			return vector * (p - start) > 0 ?
				baseLine.distanceSq(p) :
			start.distanceSq(p);
		}
	}
}
