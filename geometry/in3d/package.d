﻿/**
3D geometry package module.

Copyright: Denis Shelomovskij 2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in3d;


public:
import geometry.in3d.size;
import geometry.in3d.vector;
import geometry.in3d.point;
import geometry.in3d.box;
import geometry.in3d.curves;
import geometry.in3d.surfaces;
