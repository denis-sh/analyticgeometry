﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.curves.line;


import geometry.in2d.vector;
import geometry.in2d.point;


@safe pure nothrow @nogc:

struct Line
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Point point;
	Vector vector;

	invariant()
	{ assert(!vector.isZero); }

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in Point point, in Vector vector)
	{ this.point = point; this.vector = vector; }

	this(in Point point1, in Point point2)
	{ this(point1, point2 - point1); }

	this(in Vector normal, in real c)
	{ this((normal * (-c / normal.lengthSq)).asPoint, normal.normalCCW); }

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		/// ax + by + c, n = (a, b) = (-v.y, v.x)
		real c()
		{ return point.asVector.crossproduct(vector); }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Point opBinary(string op : "&")(in Line other)
		in { assert(vector.crossproduct(other.vector) != 0); }
		body
		{
			const det1 = -c * other.vector.x + other.c * vector.x;
			const det2 = -c * other.vector.y + other.c * vector.y;
			const det = vector.crossproduct(other.vector);
			return (Vector(det1, det2) / det).asPoint;
		}
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		real distanceSq(in Point p)
		{ return (p - point).crossproduct(vector) ^^ 2 / vector.lengthSq; }

		real signedDistance(in Point p)
		{ return (p - point).crossproduct(vector.normalized); }

		Vector projectionVector(in Point p)
		{ return (point - p).normalComponentTo(vector); }

		Line normal(in Point p)
		{ return Line(p, projectionVector(p)); }

		Point projection(in Point p)
		{ return p + projectionVector(p); }

		real getX(in real y)
		in { assert(vector.y != 0); }
		body { return (vector.x * y + c) / vector.y; }

		real getY(in real x)
		in { assert(vector.x != 0); }
		body { return (vector.y * x - c) / vector.x; }

		void setPointX(ref Point p)
		in { assert(vector.y != 0); }
		body { p.x = getX(p.y); }

		void setPointY(ref Point p)
		in { assert(vector.x != 0); }
		body { p.y = getY(p.x); }
	}
}
