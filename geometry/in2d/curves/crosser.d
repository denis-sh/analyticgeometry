﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.curves.crosser;


import std.math; // sqrt

import geometry.in2d.angle;
import geometry.in2d.vector;
import geometry.in2d.point;
import geometry.in2d.curves.line;
import geometry.in2d.curves.circle;


@safe pure nothrow @nogc:

struct CrossResult(size_t maxCount, bool canBeInfinite)
if(maxCount > 0)
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		int _count = 0;
		Point[maxCount] _points;
	}

	// Constructor
	// ----------------------------------------------------------------------------------------------------

	@disable this();

	static if(canBeInfinite)
		private this(in bool infinite)
		{
			_count = infinite ? -1 : 0;
		}

	private this(in Point[] points...)
	in { assert(points.length <= maxCount); }
	body
	{
		_count = cast(int) points.length;
		_points[0 .. _count] = points[];
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	@property const
	{
		bool empty()
		{ return !_count; }

		static if(canBeInfinite)
		{
			bool infinite()
			{ return _count == -1; }

			bool finiteNonEmpty()
			{ return _count > 0; }
		}

		static if(maxCount == 1)
		{
			Point point()
			in { static if(canBeInfinite) assert(finiteNonEmpty); else assert(!empty); }
			body
			{ return _points[0]; }
		}

		size_t points()
		in { static if(canBeInfinite) assert(!infinite); }
		body
		{ return cast(size_t) _count; }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Point opIndex(in size_t i)
		in { static if(canBeInfinite) assert(!infinite); assert(i < points); }
		body
		{
			return _points[i];
		}

		static if(!canBeInfinite)
		{
		T opCast(T : bool)()
		{ return !empty; }
		}
	}
}


CrossResult!(1, true) cross(in Line line1, in Line line2)
{
	alias Res = typeof(return);

	const det = line1.vector.crossproduct(line2.vector);

	if(!det)
		return Res(!(line2.point - line1.point).crossproduct(line2.vector));

	const det1 = -line1.c * line2.vector.x + line2.c * line1.vector.x;
	const det2 = -line1.c * line2.vector.y + line2.c * line1.vector.y;
	return Res((Vector(det1, det2) / det).asPoint);
}

CrossResult!(2, false) cross(in Line line, in Circle circle)
{
	alias Res = typeof(return);

	const rSq = circle.radiusSq;
	const cdSq = line.distanceSq(circle.centre);

	if(cdSq > rSq)
		return Res.init;

	const p = line.projection(circle.centre);

	if(cdSq == rSq)
		return Res(p);

	const v = line.vector.withLength(sqrt(rSq - cdSq));
	return Res(p - v, p + v);
}

CrossResult!(2, true) cross(in Circle circle1, in Circle circle2)
{
	alias Res = typeof(return);

	const v12 = circle2.centre - circle1.centre;
	const dSq = v12.lengthSq;

	if(!dSq)
		return Res(circle1.radius == circle2.radius);

	const r1Sq = circle1.radiusSq;
	const d1Sq = (dSq + r1Sq - circle2.radiusSq) ^^ 2 / 4 / dSq;

	if(d1Sq > r1Sq)
		return Res.init;

	if(d1Sq == r1Sq)
		return Res(circle1.centre + v12.withLength(circle1.radius));

	const p = circle1.centre + v12.withLength(sqrt(d1Sq));
	const v = v12.normal * sqrt((r1Sq - d1Sq) / dSq);
	return Res(p - v, p + v);
}
