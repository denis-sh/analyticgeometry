﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.curves.circulararc;


import std.math; // sqrt
import std.algorithm; // min

import geometry.in2d.angle;
import geometry.in2d.vector;
import geometry.in2d.point;
import geometry.in2d.box;
import geometry.in2d.basis;
import geometry.in2d.curves.circle;


@safe pure nothrow @nogc:

struct CircularArc
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Circle circle;
	Vector from, to; // normalized, around normal: [from, to] = k * normal, k >= 0

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in Circle circle, in Vector from, in Vector to)
	{ this.circle = circle; this.from = from; this.to = to; }

	this(in Point point1, in Point point2, in Angle angle)
	{
		const r = point1.distance(point2) / 2 / abs(angle.sin);
		const v = (point2 - point1).withLength(r)
			.rotated(angle.sin > 0 ? angle.complement : Angle.pi_3_2 - angle);
		const c = point1 + v;
		this(Circle(c, r), (point1 - c).normalized, (point2 - c).normalized);
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	@property const
	{
		Box box()
		{
			const circleBox = circle.box;
			auto res = Box(fromPoint, toPoint);

			if(isDirectionOn(-Vector.i))
				res.start.x = circleBox.start.x;
			if(isDirectionOn(-Vector.j))
				res.start.y = circleBox.start.y;

			if(isDirectionOn(Vector.i))
				res.end.x = circleBox.end.x;
			if(isDirectionOn(Vector.j))
				res.end.y = circleBox.end.y;

			return res;
		}

		Basis localBasis() { return Basis(circle.centre, from, to); }

		Point fromPoint() { return circle.centre + from * circle.radius; }

		Point toPoint() { return circle.centre + to * circle.radius; }

		real length() { return from.angleValueWith(to) * circle.radius; }
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		bool isDirectionOn(in Vector v)
		{
			with(localBasis.toLocal(v))
				return x >= 0 && y >= 0;
		}

		real distanceSq(in Point point)
		{
			if(isDirectionOn(point - circle.centre))
				return circle.distanceSq(point);

			return min(point.distanceSq(fromPoint), point.distanceSq(toPoint));
		}

		real distance(in Point p) { return sqrt(distanceSq(p)); }
	}
}
