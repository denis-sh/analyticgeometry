﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.curves.circle;


import std.math; // abs, PI

import geometry.in2d.vector;
import geometry.in2d.point;
import geometry.in2d.box;
import geometry.in2d.curves.segment;
import geometry.in2d.curves.line;


@safe pure nothrow @nogc:

struct Circle
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Point centre;
	real radius;

	invariant()
	{ assert(radius >= 0); }

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in Point centre, in real radius)
	{ this.centre = centre; this.radius = radius; }

	this(in Point centre, in Point pointOn)
	{ this(centre, centre.distance(pointOn)); }

	this(in Point point1, in Point point2, in Point point3)
	{
		const centre = Segment(point1, point2).midlNormal & Segment(point1, point3).midlNormal;
		this(centre, point1);
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		Point asDegenerated()
		in { assert(isDegenerated); }
		body { return centre; }

		bool isDegenerated() { return radius == 0; }

		Box box()
		{
			const v = Vector(radius, radius);
			return Box(centre - v, centre + v);
		}

		real radiusSq() { return radius * radius; }

		real length() { return PI * radius; }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Point opBinary(string op : "&")(in Ray ray)
		in { assert(ray.start.distanceSq(centre) < radiusSq); }
		body
		{
			const p = ray.baseLine.getProjection(centre);
			const d = sqrt(radiusSq - p.distanceSq(centre));
			return p + ray.vector.withLength(d);
		}
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		real signedDistance(in Point point) { return radius - centre.distance(point); }

		real distance(in Point point) { return abs(signedDistance(point)); }

		real distanceSq(in Point point) { return distance(point) ^^ 2; }

		Line getRadicalAxis(in Circle c2)
		{
			// (v-c1)²-R1² = (v-c2)²-R2² => 2*v(c2-c1) = c2²-c1²-R2²+R1² = k
			// d = c2-c1, d*v = k/2
			// A*vx+B*vy+C = 0 <=> d*v = {A,B}*v = -C <=> C = (c1²-c2²-R1²+R2²)/2
			return Line(c2.centre - centre, (centre.asVector.lengthSq - c2.centre.asVector.lengthSq - radiusSq + c2.radiusSq) / 2);
		}
	}
}
