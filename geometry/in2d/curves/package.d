﻿/**
2D curves package module.

Copyright: Denis Shelomovskij 2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.curves;


public:
import geometry.in2d.curves.line;
import geometry.in2d.curves.ray;
import geometry.in2d.curves.segment;
import geometry.in2d.curves.circle;
import geometry.in2d.curves.circulararc;
import geometry.in2d.curves.crosser;
