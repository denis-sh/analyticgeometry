﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.curves.segment;


import std.math; // sqrt
import std.algorithm; // min, swap

import geometry.in2d.vector;
import geometry.in2d.point;
import geometry.in2d.box;
import geometry.in2d.curves.line;


@safe pure nothrow @nogc:

struct Segment
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Point start, end;

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in Point start, in Point end)
	{ this.start = start; this.end = end; }

	this(in Point start, in Vector vector)
	{ this(start, start + vector); }

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		Point asDegenerated()
		in { assert(isDegenerated); }
		body { return start; }

		bool isDegenerated() { return start == end; }

		Box box() { return Box(start, end); }

		Line baseLine()
		in { assert(!isDegenerated); }
		body { return Line(start, end); }

		Vector vector() { return end - start; }

		real lengthSq() { return start.distanceSq(end); }

		real length() { return sqrt(lengthSq); }

		Point midpoint()
		{ return Point.midpoint(start, end); }

		Line midlNormal() 
		in { assert(!isDegenerated); }
		body { return Line(midpoint, vector.normal); }
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		real distanceSq(in Point p)
		{
			if(midlNormal.distanceSq(p) * 4 > lengthSq)
				return min(p.distanceSq(start), p.distanceSq(end));
			return baseLine.distanceSq(p);
		}

		Point atRatioFromStart(in real rate)
		{ return atRatioFromStart(rate, 1 - rate); }

		Point atRatioFromStart(in real rate1, in real rate2)
		{ return ((start.asVector * rate2 + end.asVector * rate1) / (rate1 + rate2)).asPoint; }

		Point atDistFromStart(in real dist)
		{ return atRatioFromStart(dist, length - dist); }

		Point atDistFromEnd(in real dist)
		{ return atRatioFromStart(length - dist, dist); }
	}
}
