/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.angle;


import std.math; // fabs, expi, sqrt


@safe pure nothrow @nogc:

Angle abs(in Angle angle)
{ return Angle(fabs(angle.sin), angle.cos); }

struct Angle
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	private real _sin, _cos;

	// Constants
	// ----------------------------------------------------------------------------------------------------

	enum Angle zero = Angle(0, 1), pi_2 = Angle(1, 0), pi = Angle(0, -1), pi_3_2 = Angle(-1, 0);

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in real sin, in real cos)
	in
	{
		enum epsilon = 1e-5;
		assert(fabs(sin) < 1 + epsilon && fabs(cos) < 1 + epsilon);
		const one = sin ^^ 2 + cos ^^ 2;
		assert(one > 1 - epsilon && one < 1 + epsilon); // rough error check
	}
	body { _sin = sin; _cos = cos; }

	this(in real val)
	{
		const c = expi(val);
		this(c.im, c.re);
	}

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static
	{
		Angle fromCos(in real cos)
		{
			if(cos ==  1)  return zero;
			if(cos == -1)  return pi;
			return Angle(sqrt(1 - cos ^^ 2), cos);
		}

		Angle fromSin(in real sin)
		{
			if(sin ==  1)  return pi_2;
			if(sin == -1)  return pi_3_2;
			return Angle(sin, sqrt(1 - sin ^^ 2));
		}

		Angle fromYX(in real y, in real x)
		{
			const d = sqrt(x ^^ 2 + y ^^ 2);
			return Angle(y / d, x / d);
		}
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		real sin() { return _sin; }

		real cos() { return _cos; }

		real signedValue() { return atan2(sin, cos); }

		real value()
		{
			const signed = signedValue;
			return signed >= 0 ? signed : 2 * PI + signed;
		}

		Angle complement()
		{ return Angle(_cos, _sin); }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Angle opUnary(string op : "-")() { return Angle(-_sin, _cos); }

		Angle opUnary(string op : "+")() { return this; }

		Angle opBinary(string op : "+")(in Angle a)
		{ return Angle(sin * a.cos + cos * a.sin, cos * a.cos - sin * a.sin); }

		Angle opBinary(string op : "-")(in Angle a)
		{ return Angle(sin * a.cos - cos * a.sin, cos * a.cos + sin * a.sin); }

		Angle opBinary(string op : "*")(in real n)
		{ return Angle(value * n); }

		Angle opBinary(string op : "/")(in real n)
		{ return Angle(value / n); }

		Angle opBinary(string op : "^^")(in real n)
		{ return Angle(value ^^ n); }
	}

	ref Angle opOpAssign(string op : "+")(in Angle a)
	{ const sin = sin; _sin = sin * a.cos + cos * a.sin; _cos = cos * a.cos - sin * a.sin; return this; }

	ref Angle opOpAssign(string op : "-")(in Angle a)
	{ const sin = sin; _sin = sin * a.cos - cos * a.sin; _cos = cos * a.cos + sin * a.sin; return this; }

	ref Angle opOpAssign(string op : "*")(in real n)
	{ return this = Angle(value * n); }

	ref Angle opOpAssign(string op : "/")(in real n)
	{ return this = Angle(value / n); }

	ref Angle opOpAssign(string op : "^^")(in real n)
	{ return this = Angle(value ^^ n); }
}
