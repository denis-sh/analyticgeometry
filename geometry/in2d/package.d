﻿/**
2D geometry package module.

Copyright: Denis Shelomovskij 2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d;


public:
import geometry.in2d.angle;
import geometry.in2d.size;
import geometry.in2d.vector;
import geometry.in2d.point;
import geometry.in2d.box;
import geometry.in2d.basis;
import geometry.in2d.curves;
import geometry.in2d.surfaces;
