﻿/**
2D surfaces package module.

Copyright: Denis Shelomovskij 2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.surfaces;


public:
import geometry.in2d.surfaces.roundedbox;
