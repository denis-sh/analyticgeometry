﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.surfaces.roundedbox;


import std.algorithm; // min, max
import std.math; // abs

import geometry.in2d.size;
import geometry.in2d.vector;
import geometry.in2d.point;
import geometry.in2d.box;


@safe pure nothrow @nogc:

struct RoundedBox
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Point start, end;
	real radius;

	invariant()
	{
		assert(start.x <= end.x && start.y <= end.y);
		assert(radius >= 0 && radius <= min(end.x - start.x, end.y - start.y));
	}

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in real x1, in real y1, in real x2, in real y2, in real radius)
	{ this.start = Point(min(x1, x2), min(y1, y2)); this.end = Point(max(x1, x2), max(y1, y2)); this.radius = radius; }

	this(in Point p1, in Point p2, in real radius)
	{ this(p1.tupleof, p2.tupleof, radius); }

	this(in Point point, in Vector vector, in real radius)
	{ this(point, point + vector, radius); }

	this(in Point point, in SizeR size, in real radius)
	{ this(point, point + Vector(size.tupleof), radius); }

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static
	{
		RoundedBox fromCentre(in Point centre, in SizeR size, in real radius)
		{ return RoundedBox(centre - Vector(size.tupleof) / 2, size, radius); }
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		Point asDegenerated()
		in { assert(isDegenerated); }
		body { return start; }

		bool isDegenerated() { return start == end; }

		Box box() { return Box(start, end); }

		real radiusSq() { return radius * radius; }

		real width() { return end.x - start.x; }

		real height() { return end.y - start.y; }

		SizeR size() { return SizeR((end - start).tupleof); }

		Point centre() { return ((start.asVector + end.asVector) / 2).asPoint; }
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		bool contains(in Point p)
		{
			if(!box.contains(p))
				return false;
			const fromCentre = p - centre;
			const v = Vector(abs(fromCentre.x) - (width - radius * 2), abs(fromCentre.y) - (height - radius * 2));
			return v.x <= 0 || v.y <= 0 || v.lengthSq <= radiusSq;
		}
	}
}
