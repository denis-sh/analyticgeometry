﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.basis;


import geometry.in2d.vector;
import geometry.in2d.point;


@safe pure nothrow @nogc:

struct Basis
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Point origin;
	Vector i, j;

	invariant()
	{ assert(i.crossproduct(j) != 0); }

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in Point origin, in Vector i, in Vector j)
	{ this.origin = origin; this.i = i; this.j = j; }

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static
	{
		Basis fromIOrtho(in Point origin, in Vector i)
		{ return Basis(origin, i, i.normal); }

		Basis fromIOrtho(in Point origin, in Vector i, in real jLength)
		{ return Basis(origin, i, i.normal.withLength(jLength)); }

		Basis fromJOrtho(in Point origin, in Vector j)
		{ return Basis(origin, j.normalCCW, j); }

		Basis fromJOrtho(in Point origin, in Vector j, in real iLength)
		{ return Basis(origin, j.normalCCW.withLength(iLength), j); }
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Vector toLocal(in Vector v)
		{
			const d = i.crossproduct(j);
			return Vector(v.crossproduct(j), i.crossproduct(v)) / d;
		}

		Point toLocal(in Point p)
		{ return toLocal(p - origin).asPoint; }

		Point toGlobal(in Point p)
		{ return origin + (p.x * i + p.y * j); }

		Vector toGlobal(in Vector v)
		{ return v.x * i + v.y * j; }
	}
}
