﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.box;


import std.algorithm; // min, max

import geometry.in2d.size;
import geometry.in2d.vector;
import geometry.in2d.point;


@safe pure nothrow @nogc:

struct Box
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	Point start, end;

	invariant()
	{ assert(start.x <= end.x && start.y <= end.y); }

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in real x1, in real y1, in real x2, in real y2)
	{ this.start = Point(min(x1, x2), min(y1, y2)); this.end = Point(max(x1, x2), max(y1, y2)); }

	this(in Point p1, in Point p2)
	{ this(p1.tupleof, p2.tupleof); }

	this(in Point point, in Vector vector)
	{ this(point, point + vector); }

	this(in Point point, in SizeR size)
	{ this(point, point + Vector(size.tupleof)); }

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static
	{
		Box fromCentre(in Point centre, in SizeR size)
		{ return Box(centre - Vector(size.tupleof) / 2, size); }
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		real width() { return end.x - start.x; }

		real height() { return end.y - start.y; }

		Vector diagonal() { return end - start; }

		SizeR size() { return SizeR(diagonal.tupleof); }

		Point centre() { return ((start.asVector + end.asVector) / 2).asPoint; }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Box opBinary(string op : "&")(in Box other)
		in { assert(crosses(other)); }
		body { return Box(max(start.x, other.start.x), max(start.y, other.start.y), min(end.x, other.end.x), min(end.y, other.end.y)); }

		Box opBinary(string op : "|")(in Box other)
		{ return Box(min(start.x, other.start.x), min(start.y, other.start.y), max(end.x, other.end.x), max(end.y, other.end.y)); }

		Box opBinary(string op : "|")(in Point p)
		{ return Box(min(start.x, p.x), min(start.y, p.y), max(end.x, p.x), max(end.y, p.y)); }
	}

	ref Box opOpAssign(string op : "&")(in Box other)
	in { assert(crosses(other)); }
	body
	{
		start = Point(max(start.x, other.start.x), max(start.y, other.start.y));
		end = Point(min(end.x, other.end.x), min(end.y, other.end.y));
		return this;
	}

	ref Box opOpAssign(string op : "|")(in Box other)
	{
		start = Point(min(start.x, other.start.x), min(start.y, other.start.y));
		end = Point(max(end.x, other.end.x), max(end.y, other.end.y));
		return this;
	}

	ref Box opOpAssign(string op : "|")(in Point p)
	{
		start = Point(min(start.x, p.x), min(start.y, p.y));
		end = Point(max(end.x, p.x), max(end.y, p.y));
		return this;
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		bool contains(in Point p)
		{ return p.x >= start.x && p.y >= start.y && p.x <= end.x && p.y <= end.y; }

		bool crosses(in Box other)
		{ return start.x <= other.end.x && end.x >= other.start.x && start.y <= other.end.y && end.y >= other.start.y; }

		real distanceSq(in Point p)
		{
			const toStart = p - start, toEnd = p - end;
			if(toStart.x < 0)
			{
				if(toStart.y < 0)
					return toStart.lengthSq;
				else if(toEnd.y > 0)
					return Vector(toStart.x, toEnd.y).lengthSq;
				else
					return toStart.x ^^ 2;
			}
			else if(toEnd.x > 0)
			{
				if(toStart.y < 0)
					return Vector(toEnd.x, toStart.y).lengthSq;
				else if(toEnd.y > 0)
					return toEnd.lengthSq;
				else
					return toEnd.x ^^ 2;
			}
			else
			{
				if(toStart.y < 0)
					return toStart.y ^^ 2;
				else if(toEnd.y > 0)
					return toEnd.y ^^ 2;
				else
					return 0;
			}
		}
	}
}
