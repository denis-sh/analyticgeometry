/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.size;


import std.algorithm; // min, max
import std.traits; // isNumeric, isIntegral, isFloatingPoint
import std.conv; // to


@safe pure nothrow @nogc:

struct Size(T) if(isNumeric!T)
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	T width, height;

	// Constants
	// ----------------------------------------------------------------------------------------------------

	enum Size empty = Size(0, 0);

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in T width, in T height)
	{ this.width = width; this.height = height; }

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static
	{
		Size square(in T length)
		{ return Size(length, length); }
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		T area()  { return width * height; }

		bool isEmpty() { return width == 0 && height == 0; }

		static if(is(isFloatingPoint!T))
		{
			Size!U truncated(U = int)() if(isIntegral!U)
			{ return Size!U(to!U(width), to!U(height)); }
		} 
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Size opBinary(string op : "+")(in Size sz2)
		{ return Size(width + sz2.width, height + sz2.height); }

		Size opBinary(string op : "-")(in Size sz2)
		{ return Size(width - sz2.width, height - sz2.height); }

		Size opBinary(string op : "&")(in Size sz2)
		{ return Size(min(width, sz2.width), min(height, sz2.height)); }

		Size opBinary(string op : "|")(in Size sz2)
		{ return Size(max(width, sz2.width), max(height, sz2.height)); }

		Size opBinary(string op : "*")(in T n)
		{ return Size(width * n, height * n); }

		Size opBinaryRight(string op : "*")(in T n)
		{ return opBinary!"*"(n); }

		Size opBinary(string op : "/")(in T n)
		{ return Size(width / n, height / n); }

		static if(isIntegral!T)
		{
			SizeR opBinary(string op : "*")(in real n)
			{ return SizeR(width * n, height * n); }

			SizeR opBinaryRight(string op : "*")(in real n)
			{ return opBinary!"*"(n); }

			SizeR opBinary(string op : "/")(in real n)
			{ return SizeR(width / n, height / n); }
		}
	}
}

alias Size!float SizeF;
alias Size!double SizeD;
alias Size!real SizeR;
alias Size!int SizeI;
