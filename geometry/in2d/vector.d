﻿/** AnalyticGeometry project file.

Copyright: Denis Shelomovskij 2011-2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module geometry.in2d.vector;


import std.math; // sqrt, atan2, acos

import geometry.in2d.angle;
import geometry.in2d.point;


@safe pure nothrow @nogc:

struct Vector
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	real x, y;

	// Constants
	// ----------------------------------------------------------------------------------------------------

	enum Vector nan = Vector(real.nan, real.nan), zero = Vector(0, 0);

	enum Vector i = Vector(1, 0), j = Vector(0, 1);

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in real x, in real y) { this.x = x; this.y = y; }

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		real isZero() { return x == 0 && y == 0; }

		real lengthSq() { return x * x + y * y; }

		real length() { return sqrt(lengthSq); }

		Angle angleXY() { return Angle.fromYX(y, x); }

		real angleSignedValueXY() { return atan2(y, x); }

		Vector normalized() { return opBinary!"/"(length); }

		/// CW normal
		Vector normal() { return Vector(-y, x); }

		/// CCW normal
		Vector normalCCW() { return Vector(y, -x); }

		Point asPoint() { return Point(this.tupleof); }
	}

	@property
	{
		real length(in real value)
		{
			opOpAssign!"*"(value / length);
			return value;
		}
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Vector opUnary(string op : "-")() { return Vector(-x, -y); }

		Vector opUnary(string op : "+")() { return this; }

		Vector opBinary(string op : "+")(in Vector v) { return Vector(x + v.x, y + v.y); }

		Vector opBinary(string op : "-")(in Vector v) { return Vector(x - v.x, y - v.y); }

		Vector opBinary(string op : "*")(in real n) { return Vector(x * n, y * n); }

		Vector opBinaryRight(string op : "*")(in real n) { return opBinary!"*"(n); }

		Vector opBinary(string op : "/")(in real n) { return Vector(x / n, y / n); }

		/// Dotproduct
		real opBinary(string op : "*")(in Vector v) { return x * v.x + y * v.y; }
	}

	ref Vector opOpAssign(string op : "+")(in Vector v) { x += v.x; y += v.y; return this; }

	ref Vector opOpAssign(string op : "-")(in Vector v) { x -= v.x; y -= v.y; return this; }

	ref Vector opOpAssign(string op : "*")(in real n) { x *= n; y *= n; return this; }

	ref Vector opOpAssign(string op : "/")(in real n) { x /= n; y /= n; return this; }

	// Functions
	// ----------------------------------------------------------------------------------------------------

	const
	{
		///[a, b]
		real crossproduct(in Vector v) { return x * v.y - y * v.x; }

		real parallelComponentToLengthSq(in Vector v) { return opBinary!"*"(v) ^^ 2 / v.lengthSq; }

		Vector parallelComponentTo(in Vector v) { return v * (opBinary!"*"(v) / v.lengthSq); }

		Vector normalComponentTo(in Vector v) { return opBinary!"-"(parallelComponentTo(v)); }

		Vector withLength(in real value) { return opBinary!"*"(value / length); }

		Angle directedAngleWith(in Vector v)
		{
			const k = sqrt(lengthSq * v.lengthSq);
			return Angle(crossproduct(v) / k, opBinary!"*"(v) / k);
		}

		Angle angleWith(in Vector v)
		{
			return abs(directedAngleWith(v));
		}

		real angleValueWith(in Vector v) { return acos(angleWith(v).cos); }

		Vector rotated(in Angle angle)
		{
			Vector res = this;
			res.rotate(angle);
			return res;
		}
	}

	void setSameLength(in Vector v) { opOpAssign!"*"(sqrt(v.lengthSq / lengthSq)); }

	void normalize() { opOpAssign!"/"(length); }

	void invert() { x = -x; y = -y; }

	void rotate(in Angle angle)
	{
		const _x = x;
		x = x * angle.cos - y * angle.sin;
		y = y * angle.cos + _x * angle.sin;
	}
}
